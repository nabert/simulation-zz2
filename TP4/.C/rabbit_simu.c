#include <stdio.h>
#include <stdlib.h>



int fibo_rabbit_simu (int month){
    int lapin =1;

    if (month == 0 || month == 1){
        return lapin;
    }

    if (month>=2){
        lapin = fibo_rabbit_simu (month-1) + fibo_rabbit_simu (month-2);
    }
    else{
        lapin = fibo_rabbit_simu (month-1);
    }

    return lapin;
}


int main(){


    // Simulation simple se basant sur fibonacci du nombre de lapin selon le mois

    for (int i = 0; i<10; i++){
        printf("%d\n", fibo_rabbit_simu (i));

    }




}