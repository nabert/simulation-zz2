import os
import numpy as np
import matplotlib.pyplot as plt


def extract_data_from_txt(file_path):
    if not os.path.isfile(file_path):
        raise FileNotFoundError(f"No such file: '{file_path}'")
    data = np.loadtxt(file_path)
    return data

def plot_data(data, abscisse=None, ordinate=None, title=None, relative_path =None):
    for col in range(data.shape[0]):
        plt.plot(data[col, :], label=f'Simulation {col+1}')
    
    mean_data = np.mean(data, axis=0)
    plt.plot(mean_data, label='Moyenne', linestyle='-.', color='black')
    
    plt.title(title)

    plt.xlabel(abscisse)
    plt.ylabel(ordinate)
    
    plt.legend()
    plt.savefig(relative_path+title+'.png', dpi=300)  # Enregistre en PNG avec une résolution de 300 DPI
    plt.show()

""" file_path = '../.txt/bunny_on_year_1.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Number of Bunny', 'Number_Bunny_On_Year_10','../rapport/assets/natality/')

file_path = '../.txt/bunny_on_year_2.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Number of Bunny ', 'Number_Bunny_On_Year_100','../rapport/assets/natality/')
    
file_path = '../.txt/bunny_on_year_3.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Number of Bunny', 'Number_Bunny_On_Year_1000','../rapport/assets/natality/')



file_path = '../.txt/death_on_year_1.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Number of Death ', 'Number_Of_Death_On_Year_10','../rapport/assets/mortality/')

file_path = '../.txt/death_on_year_2.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Number of Death ', 'Number_Of_Death_On_Year_100','../rapport/assets/mortality/')

file_path = '../.txt/death_on_year_3.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Number of Death ', 'Number_Of_Death_On_Year_1000','../rapport/assets/mortality/')





file_path = '../.txt/time_for_simulation_1.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Time (ms)', 'Time_For_Simulation_On_Year_10','../rapport/assets/init_pop_size/')

file_path = '../.txt/time_for_simulation_2.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Time (ms)', 'Time_For_Simulation_On_Year_100','../rapport/assets/init_pop_size/')

file_path = '../.txt/time_for_simulation_3.txt'
data = extract_data_from_txt(file_path)
plot_data(data, 'Month', 'Time (ms)', 'Time_For_Simulation_On_Year_1000','../rapport/assets/init_pop_size/')

 """

def NbRabbit():

    file_path = '../.txt/bunny_on_year_1.txt'
    data = extract_data_from_txt(file_path)
    file_path = '../.txt/death_on_year_1.txt'
    data -= extract_data_from_txt(file_path)
    mean_data = np.cumsum(np.mean(data, axis=0))


    plt.plot(mean_data, label='10 init R', linestyle='-.', color='red')


    file_path = '../.txt/bunny_on_year_2.txt'
    data = extract_data_from_txt(file_path)
    file_path = '../.txt/death_on_year_2.txt'
    data -= extract_data_from_txt(file_path)
    mean_data = np.cumsum(np.mean(data, axis=0))

    plt.plot(mean_data, label='100 init R', linestyle='-.', color='blue')


    file_path = '../.txt/bunny_on_year_3.txt'
    data = extract_data_from_txt(file_path)
    file_path = '../.txt/death_on_year_3.txt'
    data -= extract_data_from_txt(file_path)
    mean_data = np.cumsum(np.mean(data, axis=0))

    plt.plot(mean_data, label='1000 init R', linestyle='-.', color='green')


    plt.title('Number of Rabbit on month')
    plt.xlabel('Month')
    plt.ylabel('Number of Rabbit')
    plt.legend()


    plt.savefig('../rapport/assets/Number_Rabbit.png', dpi=300)
    plt.show()

NbRabbit()