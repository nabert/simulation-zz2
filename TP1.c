#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


#define SEED 5;





void NonLinear_mst(int * seed){ //middle square technique algorithme 4 bits digit

    int seedSqr = (* seed) * (* seed);

    seedSqr = seedSqr/100;// décalage à droite (suppression des deux digits de droite par proprieteé de l'int en C)
    seedSqr = seedSqr%10000; // modulo 10000 pour supprimer les eux digit de gauches après décalage

    * seed = seedSqr;
    
}

void NeumannGenerator (){

    int seed = SEED;
    int precseed = seed;
    int i;

    for (i=0; i<100; i++) {
        
        NonLinear_mst(&seed);
        printf("%04d\n", seed);

        if (precseed == seed){ // On verifie si la suite ne s'écrase pas => seed precedente = seed courante
            printf("absorbant à l'itération %d\n", i);
            i = 100;
        }

        precseed = seed;

    }

    

}

void CoinTossing (int nbOfToss){

    int numberOf1 =0, numberOf0 =0;
    int i =0;
    int toss;

    for (i=0; i<nbOfToss; i++){

        toss = rand()%2;
        printf("%d\n", toss);

        if (toss == 1){
            numberOf1 += 1;
        }
        else{
            numberOf0 += 1;
        }

    }

    printf("fréquence de 0 est %f\n", (float) numberOf0/nbOfToss);
    printf("fréquence de 1 est %f\n", (float) numberOf1/nbOfToss);


}



void DisplayDiceTossing(float * tabEvent, int nbOfDiceFace){

    int i = 0;

    while (i<nbOfDiceFace){

        printf(" le chiffre %d est apparu avec une fréquence de %f\n", i+1, tabEvent[i]);
        i++;
    }

}

void DiceTossing ( int nbOfDiceToss, int nbOfDiceFace){

    float * tabEvent = (float *) malloc(sizeof(float) * nbOfDiceFace);
    int i = 0;
    int toss = 0; 

    for (i=0; i<nbOfDiceFace; i++){

        tabEvent[i] = 0.;

    }

    for (i=0; i<nbOfDiceToss; i++){

        toss = rand()%nbOfDiceFace;
        tabEvent[toss]+=1.;

    }

    for (i=0; i<nbOfDiceFace; i++){

        tabEvent[i] = (float) (tabEvent[i]/nbOfDiceToss);
    }

    DisplayDiceTossing(tabEvent, nbOfDiceFace);

    free(tabEvent);

}


void intRand (int nbOfIterations, int a, int c, int m, double * tabRand){

    int x0 = SEED;
    int x_next = 0;
    int compteur = 1;


    tabRand [0] = x0; 

    while (compteur < nbOfIterations){
        x_next =  (a * x0 + c) % m;
        x0 = x_next;
        tabRand[compteur] = x0;
        compteur++;
    }

}



void floatRand (){


    int a = 5, c = 1, m = 16;
    int nbOfIterations = 32;
    double * tabRand = (double *) malloc(sizeof(double) * nbOfIterations);
    

    for (int i = 0; i<nbOfIterations;i++){
        tabRand[i] = 0;
    }


    intRand (nbOfIterations, a, c, m , tabRand) ;

    for (int i = 0; i < nbOfIterations; i++){
        tabRand[i]=  (tabRand[i]) / m;
        printf("%f\n", tabRand[i]);

    }




}










int main(){

    //NeumannGenerator();

    //CoinTossing(100);

    //DiceTossing(100,10);

    //intRand(32,5,1,16);

    //floatRand();

}


