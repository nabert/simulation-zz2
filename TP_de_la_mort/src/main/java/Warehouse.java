package main.java;
// Warehouse.java
public class Warehouse implements Positionable {
    private int id;
    private static int classId = 0;
    private int x, y;

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }


    public Warehouse( int x, int y) {
        this.id = classId;
        classId++;
        this.x = x;
        this.y = y;
    }

    public int getX() { return x; }
    public int getY() { return y; }

            // Getter for id
    public int getId() {
        return id;
    }

    // Setter for id
    public void setId(int id) {
        this.id = id;
    }
}
