package main.java;
    // Positionable.java
public interface Positionable {
    int getX();
    int getY();
}
