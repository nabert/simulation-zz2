// Client.java
package main.java;


public class Client implements Positionable {
private int id;
private static int classId = 0;
private int x, y;
private Warehouse assignedWarehouse;

public Client(int x, int y, Warehouse assignedWarehouse) {
    this.id = classId;
    classId++;
    this.x = x;
    this.y = y;
    this.assignedWarehouse = assignedWarehouse;
}

@Override
public String toString() {
    return "(" + x + ", " + y + ")";
}


public int getX() 
{ 
return x; 
}

public int getY() 
{ 
return y; 
}
public Warehouse getAssignedWarehouse() 
{
return assignedWarehouse; 
}

public int getId() 
{
return id;
}

public void setId(int id) 
{
this.id = id;
}

public void setX(int x) 
{
this.x = x;
}

public void setY(int y) 
{
this.y = y;
}

public void setAssignedWarehouse(Warehouse assignedWarehouse) 
{
this.assignedWarehouse = assignedWarehouse;
}
}
