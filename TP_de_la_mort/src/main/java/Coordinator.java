// Coordinator.java
package main.java;

import java.util.*;
import java.util.concurrent.*;

import main.java.Client;

public class Coordinator {
    private List<Client> clients = new ArrayList<>();
    private List<Client> clients_en_livraison = new ArrayList<>();
    private List<Drone> drones = new ArrayList<>();
    private List<Warehouse> warehouses = new ArrayList<>();
    private ScheduledExecutorService executorUpdate = Executors.newScheduledThreadPool(1);
    private int grille_size = 20;

    public Coordinator(int numDrones, int numWarehouses) {
        for (int i = 0; i < numDrones; i++) {
            drones.add(new Drone(i, i, 0));
        }
        for (int i = 0; i < numWarehouses; i++) {
            warehouses.add(new Warehouse(0, i+1));
        }
    }

    public Coordinator(int numDrones, int numWarehouses, int grille_size) {
        Random rand = new Random();

        for (int i = 0; i < numDrones; i++) {
            drones.add(new Drone(i, i, 0));
        }
        for (int i = 0; i < numWarehouses; i++) {
            warehouses.add(new Warehouse(rand.nextInt(grille_size), rand.nextInt(grille_size)));
        }
        this.grille_size = grille_size;
    }

    // Generate clients with a specified warehouse for their product
    public void startClientGeneration() {
        executorUpdate.scheduleAtFixedRate(() -> {
            Random rand = new Random();
            int id = clients.size() + 1;
            int x, y;
/*             int x = rand.nextInt(20);
            int y = rand.nextInt(20); */
            System.out.println("Génération d'un client");
            boolean positionOccupied;
            do {
                positionOccupied = false;
                x = rand.nextInt(grille_size);
                y = rand.nextInt(grille_size);
                for (Client client : clients) {
                    if (client.getX() == x && client.getY() == y) {
                        positionOccupied = true;
                        break;
                    }
                }
                if (!positionOccupied) {
                    for (Drone drone : drones) {
                        if (drone.getX() == x && drone.getY() == y) {
                            positionOccupied = true;
                            break;
                        }
                    }
                }
                if (!positionOccupied) {
                    for (Warehouse warehouse : warehouses) {
                        if (warehouse.getX() == x && warehouse.getY() == y) {
                            positionOccupied = true;
                            break;
                        }
                    }
                }
            } while (positionOccupied);


            Warehouse warehouse = warehouses.get(rand.nextInt(warehouses.size())); // Random warehouse
            clients.add(new Client(x, y, warehouse));

        }, 0, 4, TimeUnit.SECONDS);
    }

public void updateSimulation() {
        
    executorUpdate.scheduleAtFixedRate(() -> {

        for (Drone drone : drones) {
            if (!drone.isAvailable()) {
                // If the drone is not available, it's currently working on a task
                if (drone.getTarget() != null) {
                    drone.moveToTarget(drones, grille_size);

                    // Check if the drone has reached its target
                    if (drone.getX() == drone.getTarget().getX() && drone.getY() == drone.getTarget().getY()) {
                        if (drone.isPickingUp()) {
                            // Drone has reached the warehouse, now move to the client
                            drone.setPickingUp(false); // Task shifts to delivery
                            drone.setTarget(drone.getAssignedClient()); // Update target to the client
                        } else {
                            // Drone has completed the delivery
                            clients_en_livraison.remove(drone.getAssignedClient());
                            drone.setAvailable(true); // Mark drone as available
                            drone.setAssignedClient(null); // Clear the client assignment
                            drone.setTarget(null); // Clear the target
                        }
                    }
                }
            } else {
                // If the drone is available, assign it a new client
                if (!clients.isEmpty()) {
                    
                    System.out.println("Attribution d'un client à un drone");

                    Client client = findNearestClient(drone);
                    clients.remove(client); // Remove client from queue
                    clients_en_livraison.add(client);
                    drone.setAssignedClient(client); // Assign the client to the drone
                    drone.setTarget(client.getAssignedWarehouse()); // First target is the warehouse
                    drone.setPickingUp(true); // Drone is in pickup mode
                    drone.setAvailable(false); // Drone is now busy
                }
            }
        }
        }, 0, 1, TimeUnit.SECONDS);
    }

    public static List<Client> addAllIfNotNull(List<Client> list, List<Client> listToAdd) {
        List<Client> listTemp = list;
        for (int i = 0; i < listToAdd.size(); i++) {
            if (listToAdd.get(i) != null) {
                listTemp.add(listToAdd.get(i));
            }
        }
        System.out.println("Liste des clients : " + listTemp);
        return listTemp;
    }

        private Client findNearestClient(Drone drone) {
            return clients.stream()
                    .min(Comparator.comparingInt(client -> Math.abs(client.getX() - drone.getX()) + Math.abs(client.getY() - drone.getY())))
                    .orElse(null);
        }


        public List<Client> getClients() {
            return clients;
        }

        public void setClients(List<Client> clients) {
            this.clients = clients;
        }

        public List<Drone> getDrones() {
            return drones;
        }

        public void setDrones(List<Drone> drones) {
            this.drones = drones;
        }

        public List<Warehouse> getWarehouses() {
            return warehouses;
        }

        public void setWarehouses(List<Warehouse> warehouses) {
            this.warehouses = warehouses;
        }

        public ScheduledExecutorService getExecutor() {
            return executorUpdate;
        }

        public void setExecutor(ScheduledExecutorService executor) {
            this.executorUpdate = executor;
        }

        public List<Client> getClients_en_livraison() {
            return clients_en_livraison;
        }

        public void setClients_en_livraison(List<Client> clients_en_livraison) {
            this.clients_en_livraison = clients_en_livraison;
        }

        public int getGrille_size() {
            return grille_size;
        }

        public void setGrille_size(int grille_size) {
            this.grille_size = grille_size;
        }

}
