package com;
// SimulationState.java
import java.util.List;

import main.java.Client;
import main.java.Drone;
import main.java.Warehouse;


public class SimulationState {
    private List<Client> clients;
    private List<Drone> drones;
    private List<Warehouse> warehouses;
    private int grille_size;

    public SimulationState(List<Client> clients, List<Drone> drones, List<Warehouse> warehouses, int grille_size) {
        this.clients = clients;
        this.drones = drones;
        this.warehouses = warehouses;
        this.grille_size = grille_size;
    }

    public List<Client> getClients() { return clients; }
    public List<Drone> getDrones() { return drones; }
    public List<Warehouse> getWarehouses() { return warehouses; }
    public int getGrille_size() { return grille_size; }
}
