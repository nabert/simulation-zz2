package com;
// SimulationController.java
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;

import main.java.Client;
import main.java.Coordinator;   
import main.java.Drone;
import main.java.Warehouse;
import scala.jdk.CollectionConverters;



@Controller
//@RestController
@RequestMapping("/")
public class SimulationController {
    private final Coordinator coordinator;

    public SimulationController() {
        this.coordinator = new Coordinator(5, 3, 30); // 5 drones, 3 entrepôts
        coordinator.startClientGeneration();
        System.out.println("Simulation controller initialized");
        coordinator.updateSimulation();
    }

    // API pour récupérer l'état de la simulation (JSON)
    @GetMapping("/state")
    @ResponseBody
    public SimulationState getState() {
        List<Client> tempClients = new ArrayList<>();
        tempClients.addAll(coordinator.getClients_en_livraison());
        tempClients.addAll(coordinator.getClients());
        return new SimulationState(tempClients, coordinator.getDrones(), coordinator.getWarehouses(), coordinator.getGrille_size());
    }
    @PostMapping("/pauseSimulation")
    @ResponseBody
    public void suspendTasks() {
        if (coordinator.getExecutor() != null) {
            coordinator.getExecutor().shutdown(); // Annule la tâche 1
        }
        System.out.println("Tasks suspended");
    }

    // Relancer les tâches
    public void restartTasks() {
        // Replanifier les tâches après suspension
        coordinator.startClientGeneration();
        System.out.println("Simulation controller initialized");
        coordinator.updateSimulation();
        System.out.println("Tasks restarted");
    }




    @GetMapping("/")
    public String showSimulation(Model model) {
        System.out.println("Rendering simulation page...");
        model.addAttribute("clients", coordinator.getClients());
        //addAll(coordinator.getClients_en_livraison())
        model.addAttribute("drones", coordinator.getDrones());
        model.addAttribute("warehouses", coordinator.getWarehouses());
        model.addAttribute("grille_size", coordinator.getGrille_size());

        return "simulation"; // Assurez-vous que "simulation" correspond au nom de la vue Thymeleaf
    }
}
