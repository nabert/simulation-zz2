package main.java;
import java.util.List;
// Drone.java
public class Drone {
    private int id;
    private int x, y;
    private boolean available;
    private boolean pickingUp; // True si le drone va au dépôt
    private Positionable target; // Maintenant `target` est un `Positionable`
    private Client assignedClient;

    public Drone(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.available = true;
        this.pickingUp = false;
    }

    public void moveToTarget(List<Drone> drones, int grille_size) {
        int nextX = x, nextY = y;
        if (target != null) {

            if (x < target.getX() ) nextX = x + 1;
            else if (x > target.getX()) nextX = x - 1;

            if (y < target.getY()) nextY = y + 1;
            else if (y > target.getY()) nextY = y - 1;

            for (int i = 0 ; i < drones.size() ; i++) {
                if (drones.get(i).getX() == nextX && drones.get(i).getY() == nextY) {
                    nextX = (x+1 < grille_size)? x+1 : x-1;
                    nextY = (y-1 > 0) ? y-1 : y+1;
                }
            }

            x = nextX;
            y = nextY;
        }
    }

    public Positionable getTarget() { return target; }
    public void setTarget(Positionable target) { this.target = target; }

        // Getters and setters
    public boolean isAvailable() { return available; }
    public void setAvailable(boolean available) { this.available = available; }

    public boolean isPickingUp() { return pickingUp; }
    public void setPickingUp(boolean pickingUp) { this.pickingUp = pickingUp; }

    public Client getAssignedClient() { return assignedClient; }
    public void setAssignedClient(Client assignedClient) { this.assignedClient = assignedClient; }

    public int getX() { return x; }
    public int getY() { return y; }

        // Getter for id
    public int getId() {
        return id;
    }

    // Setter for id
    public void setId(int id) {
        this.id = id;
    }

}
