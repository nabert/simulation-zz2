import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def plot_ods_dice(file_path):
    # Read the ODS file, 
    data = pd.read_excel(file_path)
    print(data)

    x_axis = data.iloc[:, 1]  # Assuming column B is the second column
    y_axis = data.iloc[:, 0]  # Assuming column A is the first column
    z_axis = data.iloc[:, 2]  # Assuming column C is the third column (if exists)


    # Plot the data
    plt.plot(x_axis, y_axis, 'x', markeredgewidth=2)
    
    plt.title('Average mean for 40 times dice throws 10³ repetitions\n241 bins around theorical means = 3.5')
    plt.xlabel('means')
    plt.ylabel('frequency')
    plt.legend(loc='best')
    plt.show()


def plot_ods_BAM(file_path):
    # Read the ODS file, 
    data = pd.read_excel(file_path)
    print(data)

    x_axis = data.iloc[:, 1]  # Assuming column B is the second column
    y_axis = data.iloc[:, 0]  # Assuming column A is the first column


    # Plot the data
    plt.plot(x_axis, y_axis, 'x', markeredgewidth=2)
    
    plt.title('Box and Muller plot for 1 000 000 throws on 20 bins')
    plt.xlabel('means')
    plt.ylabel('frequency')
    plt.legend(loc='best')
    plt.show()


def plot_uniform(file_path):
    data = np.loadtxt(file_path, delimiter=",")
    plt.figure()
    plt.ylabel('Values')
    plt.xlabel('generation n°X')
    plt.title("Values of uniform generator for each generation from 1 to 100")
    plt.scatter(range(len(data)), data, marker = 'o')
    plt.show()

def plot_neg_exp(file_path):
    data = np.loadtxt(file_path)
    plt.figure()
    plt.ylabel('frequency')
    plt.xlabel('values')
    plt.title("Frequencies of the negExp function in function of output values")
    plt.scatter(range(len(data)), data, marker = 'x')
    plt.show()


if __name__ == "__main__":
    file_path1 = "./.xlsx/Dice_Sim.xlsx" 
    file_path2 = "./.xlsx/BAM_Simu.xlsx"  
    file_path3 = "./.txt/uniform_plot.txt"
    file_path4 = "./.txt/neg_exp_distrib.txt"

    plot_uniform(file_path3)
    plot_neg_exp(file_path4)
    plot_ods_dice(file_path1)
    plot_ods_BAM(file_path2)



