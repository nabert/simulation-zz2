/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)  
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

/*compil line : gcc mt19937ar.c -o mt -Wall -Wextra -g -lm */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */
#define PI 3.14159265359

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* initializes mt[N] with a seed */
void init_genrand(unsigned long s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] = 
	    (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array(unsigned long init_key[], int key_length)
{
    int i, j, k;
    init_genrand(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32(void)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }
  
    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long genrand_int31(void)
{
    return (long)(genrand_int32()>>1);
}

/* generates a random number on [0,1]-real-interval */
double genrand_real1(void)
{
    return genrand_int32()*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double genrand_real2(void)
{
    return genrand_int32()*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double genrand_real3(void)
{
    return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53(void) 
{ 
    unsigned long a=genrand_int32()>>5, b=genrand_int32()>>6; 
    return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 
/* These real versions are due to Isaku Wada, 2002/01/09 added */

// 2) Generation of uniform random numbers between A and B


void uniform (double a, double b){

    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;  
    //unsigned long init[4]={0x123, 0x274, 0x945, 0x446}, length=4;  
    init_by_array(init, length);
    for (int i = 0; i < 100; i++){
        if (i%10 == 0) printf ("\n");
        printf("%2.1f  ", genrand_real1()*(b-a) + a  );
    }

}



/* Generate nb_drawing drawing of number between 0 and 1 and display distribution of those numbers in [0,0.35];]0.35,0.8];]0.8,1]*/
void discrete_distribution(int nb_drawings){
    double indiv = 0;
    int distribution[3]= {0,0,0};

    for (int i =0 ; i < nb_drawings; i++){
        indiv = genrand_real1();
        //printf ("%1.3f", indiv);

        if (indiv <= 0.35) {distribution[0]+=1;}
        else { 
            if (indiv <= 0.8) {distribution[1]+=1;}
            else {distribution[2]+=1;}
            }
        

    }

    printf("\nSur %d évènements\n", nb_drawings);
    for (int i =0 ; i < 3; i++ ){
        printf ("la distribution de l'évènement %d est de %f\n ", i+1, (double) (distribution[i])/nb_drawings);
    }


}

void display_array(int array_size, double * array){

    for (int i = 0; i < array_size; i++){

        printf("les individu de la classe %d ont une probabilité d'apparition de %2.5f\n", i+1, array[i] );

    }
    printf("\n");
}


void class_simulation(int array_size, int nb_drawings, int * distribution){

    double indiv = 0;
    //int distribution[array_size];
    double frequency = (1/(double) array_size);


    for (int i = 0; i < (nb_drawings/array_size); i++){
        indiv = genrand_real1();
        for (int j = 0; j < array_size; j++){
            if (indiv >= j * frequency && indiv < j+1 * frequency ) distribution[j]+=1;
        }
    }
}



void generic_discrite_distrib(int array_size, int * array, double * class_proba, double * cumulativ_proba){

    int tot_individual = 0;





    for (int i = 0; i < array_size;i++ ){
        tot_individual += array[i];
    }


    for (int i = 0; i < array_size; i++){
        class_proba[i] = ((double) array[i]/ (double) tot_individual);
        cumulativ_proba[i] = (i>0? cumulativ_proba[i-1]:0)+ class_proba[i];
    }




}

double neg_exp(double mean){
    return -mean * log(genrand_real1());
}


double neg_exp_simu(int nb_drawings, double mean){
    int sum = 0;
    for (int i = 0; i < nb_drawings; i++){
        sum += neg_exp(mean);
    }
    return ((double)sum / (double) nb_drawings);
}


void neg_exp_distrib(int * Test22bins, int nb_drawings, double mean){
    double neg = 0;
    for (int i = 0; i < nb_drawings; i++){
        neg = neg_exp(mean);
        (((int)neg > 20)? Test22bins[20]++ : Test22bins[(int)neg]++);
        Test22bins[(int)neg] += 1;
    }
}

void display_neg_exp_distrib(int * Test22bins){

    for (int i = 0; i < 20; i++){
        
        printf("son apparues %d occurences du nombre %d\n", Test22bins[i], i);

    }    

  printf("son apparues %d occurences du nombre 20+\n\n", Test22bins[20]);
    
}


void average_dice (int nb_drawing, double * mean){
    int sum = 0;
    for (int i = 0; i < nb_drawing; i++){
        sum += genrand_int32()%6 + 1;
    }
    *mean = (double) sum / (double) nb_drawing;
    *mean = round(100.*(*mean))/100.;
}

void array_average_dice(int nb_drawing, double * mean, double * array, int array_size, double * array_mean){

    for (int i= 0; i < array_size; i++){
        average_dice(nb_drawing,mean);
        
        array[i] = *mean;
        *array_mean += *mean;

    }

    *array_mean = *array_mean/((double) array_size);

}






// a function taking an array, its length and display them on an excel file
void array_to_excel(int * freq_array, int array_size, char * file_name, double array_mean, double * dice_array){
    FILE * file = fopen(file_name, "w");
    fprintf(file, "%d,%f ,%f\n", freq_array[0],dice_array[0], array_mean);

    for (int i = 1; i < array_size; i++){
        fprintf(file, "%d, %f \n", freq_array[i], dice_array[i]);
    }
    fclose(file);
}


// a function that takes an array of double and create a frequency distribution of the values in the array around the mean

void array_to_freq(double * array, int array_size,  int * freq_array, int freq_array_size, double min_val, double max_val){
    
    // for(int i = 0; i <20; i++){
    //     printf("%d\n", freq_array[i]);
    // }
    double freq = 0;
    double freq_step = ((max_val-min_val)/(double) freq_array_size);
    //printf("%f frequence\n", freq_step);
    for (int i = 0; i < array_size; i++){
        freq = array[i];
        //printf("%f\n", freq);

        for (int j = 0; j < freq_array_size; j++){

            if (
            freq >= min_val + (((double)j)*freq_step) &&   //(min_val+(double) (j+(min_val<0? (int)-min_val:(int) min_val))*freq_step) && 
            freq <  min_val + (((double)j+1)*freq_step)){//(min_val+(double) (j+1+(min_val<0? (int)-min_val:(int) min_val))*freq_step)) {
                //printf("%f frequence\n", min_val + (((double)j)*freq_step));

                freq_array[j]++; 
            }
            array[j]=  min_val + (((double)j)*freq_step);  
        }

    }


}


void box_and_muler(double * Rn1, double * Rn2){

    *Rn1 = (genrand_real2()-1)*(-1); // we are sure that *Rn1 is in (0,1], because ln(0) is undefined
    *Rn2 = (genrand_real2()-1)*(-1); // we are sure that *Rn1 is in (0,1], because ln(0) is undefined


    *Rn1 = cos(2*PI*(*Rn2))*pow((-2*log(*Rn1)),(double)(1/2));
    *Rn2 = sin(2*PI*(*Rn2))*pow((-2*log(*Rn1)),(double)(1/2));

    *Rn1 *= 5;
    *Rn2 *= 5;


    //printf("%f , %f\n",*Rn1,*Rn2 );


}

void box_muler_array(double * array, int nb_iter, double * Rn1, double * Rn2){

    for (int i = 0; i < (nb_iter/2); i++){
        box_and_muler(Rn1, Rn2);
        array[2*i] = *Rn1;
        array[(2*i)+1] = *Rn2;
    }
}




int main(void)
{
/*
    int i;
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    printf("1000 outputs of genrand_int32()\n");
    for (i=0; i<1000; i++) {
      printf("%10lu ", genrand_int32());
      if (i%5==4) printf("\n");
    }
    printf("\n1000 outputs of genrand_real2()\n");
    for (i=0; i<1000; i++) {
      printf("%10.8f ", genrand_real2());
      if (i%5==4) printf("\n");
    }
/////////////////////////////////////////////////////////////////////////////////////////////

    printf("100 output of numbers between -98 and +57.5");
    uniform(-98,57.7);

////////////////////////////////////////////////////////////////////////////////////////////

    int nb_drawings = 1000;
    discrete_distribution(nb_drawings);

    nb_drawings = 10000;
    discrete_distribution(nb_drawings);

    nb_drawings = 100000;
    discrete_distribution(nb_drawings);

    nb_drawings = 1000000;
    discrete_distribution(nb_drawings);

////////////////////////////////////////////////////////////////////////////////////////////

    int array_size = 5;
    int * distribution;
    double * class_proba;
    double * cumulativ_proba;
    distribution = calloc(array_size, sizeof(int));
    class_proba = calloc(array_size, sizeof(double));
    cumulativ_proba = calloc(array_size, sizeof(double));



    class_simulation(array_size, 1000 ,distribution);
    generic_discrite_distrib( array_size, distribution, class_proba, cumulativ_proba);


    printf("Probabilités de classes pour %d classes :\n", array_size);
    display_array(array_size,class_proba );
    printf("Probabilités cumulées de classes pour %d classes :\n", array_size);
    display_array(array_size, cumulativ_proba );


////////////////////////////////////////////////////////////////////////////////////////////
    //4.b
    nb_drawings = 1000;
    int mean = 10;
    printf("Simulation de %d évènements de loi exponentielle de moyenne %d\n", nb_drawings, mean);
    printf("La somme des évènements est de %f\n", neg_exp_simu(nb_drawings, mean));
    
    nb_drawings = 1000000;
    printf("Simulation de %d évènements de loi exponentielle de moyenne %d\n", nb_drawings, mean);
    printf("La somme des évènements est de %f\n", neg_exp_simu(nb_drawings, mean));



////////////////////////////////////////////////////////////////////////////////////////////

    
    int * Test22bins = calloc(21, sizeof(int));


    nb_drawings = 1000;
    mean = 10;
    neg_exp_distrib(Test22bins, nb_drawings, mean);
    display_neg_exp_distrib(Test22bins);



    for (int i = 0; i < 21; i++){
        Test22bins[i] = 0;
    }

    nb_drawings = 1000000;
    mean = 10;
    neg_exp_distrib(Test22bins, nb_drawings, mean);
    display_neg_exp_distrib(Test22bins);


////////////////////////////////////////////////////////////////////////////////////////////



    double avg = 0;
    nb_drawings = 1000000;
    average_dice(nb_drawings, &avg);
    printf("La moyenne de la somme des dés est de %f\n", avg);


*/

////////////////////////////////////////////////////////////////////////////////////////////
/*
    double avg = 0;
    double array_mean = 0;
    double * dice_array;
    int * freq_array;


    dice_array = calloc(1000, sizeof(double));
    freq_array = calloc(241, sizeof(int));



    array_average_dice(1000, &avg,dice_array, 1000, &array_mean);

    array_to_freq(dice_array, 1000, freq_array, 241, 0,42);

    array_to_excel(freq_array, 241, "Dice_Sim.xlsx", array_mean, dice_array);


    printf("%f\n",dice_array[0]);
    printf("%f\n",dice_array[240]);
*/
////////////////////////////////////////////////////////////////////////////////////////////


    double Rn1 = 0;
    double Rn2 = 0;

    int nb_iter = 1000000;
    int bins = 20;

    double * array_BAM;

    int * freq_array;

    freq_array = calloc(20, sizeof(int));



    array_BAM = calloc(nb_iter, sizeof(double));

    box_muler_array(array_BAM, nb_iter, &Rn1, &Rn2);

    array_to_freq(array_BAM, nb_iter, freq_array, bins, -5, 5);

    array_to_excel(freq_array,bins,"BAM_Simu.xlsx",0,array_BAM);


    //https://github.com/DEShawResearch/random123
    //https://docs.oracle.com/javase/8/docs/api/java/util/Random.html

    return 0;
}

