/* -------------------------------------------------------------------- */
/* fichier : mt_TP2.c                                                  */
/* rôle    : Simulation de tirages aléatoires et distributions          */
/* -------------------------------------------------------------------- */

#include <stdio.h>
#include "mt_TP2.c"
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265359

/* -------------------------------------------------------------------- */
/* uniform                                                              */
/*                                                                      */
/* Génère 100 nombres aléatoires uniformément distribués dans l'        */
/* intervalle [a, b].                                                   */
/*                                                                      */
/* En entrée :                                                          */
/*   - double a : borne inférieure de l'intervalle                      */
/*   - double b : borne supérieure de l'intervalle                      */
/*                                                                      */
/* En sortie :                                                          */
/*   - Affiche les 100 nombres générés avec une précision               */
/*     de 1 chiffre après la virgule.                                   */
/* -------------------------------------------------------------------- */

void uniform(double a, double b) {
    unsigned long long init[4] = {0x123, 0x234, 0x345, 0x456}; // Clés d'initialisation
    unsigned long long length = 4; // Longueur de la clé

    init_by_array64(init, length); // Initialisation du générateur

    for (int i = 0; i < 100; i++) {
        //if (i % 10 == 0) printf("\n"); // Saut de ligne toutes les 10 valeurs
        printf("%2.1f \n ", genrand64_real1() * (b - a) + a); // Génération et affichage d'une valeur
    }

}

/* -------------------------------------------------------------------- */
/* discrete_distribution                                                */
/*                                                                      */
/* Simule une distribution discrète à partir d'un nombre donné de       */
/* tirages aléatoires entre 0 et 1.                                     */
/*                                                                      */
/* En entrée :                                                          */
/*   - int nb_drawings : nombre de tirages à effectuer                  */
/*                                                                      */
/* En sortie :                                                          */
/*   - Affiche la distribution des événements.                          */
/* -------------------------------------------------------------------- */


void discrete_distribution(int nb_drawings) {
    double indiv = 0; // Variable pour stocker la valeur générée
    int distribution[3] = {0, 0, 0}; // Tableau pour compter les occurrences des événements
    int classes[3] = {'A','B','C'};
    for (int i = 0; i < nb_drawings; i++) {
        indiv = genrand64_real1(); // Génération d'un nombre aléatoire

        if (indiv <= 0.35) {
            distribution[0] += 1; // Incrémentation pour le premier événement
        } else if (indiv <= 0.8) {
            distribution[1] += 1; // Incrémentation pour le deuxième événement
        } else {
            distribution[2] += 1; // Incrémentation pour le troisième événement
        }
    }

    printf("\nSur %d évènements\n", nb_drawings); // Affichage du nombre total d'événements

    for (int i = 0; i < 3; i++) {
        printf("La distribution de l'évènement %c est de %f\n\n", classes[i], (double)(distribution[i]) / nb_drawings); // Affichage de la proportion pour chaque événement
    }
}

/* -------------------------------------------------------------------- */
/* display_apparition                                                   */
/*                                                                      */
/* Affiche les probabilités d'apparition pour chaque classe dans un     */
/* tableau.                                                             */
/*                                                                      */
/* En entrée :                                                          */
/*   - int array_size : taille du tableau                               */
/*   - double *array : tableau des probabilités                         */
/*                                                                      */
/* En sortie :                                                          */
/*   - Affiche les probabilités d'apparition.                           */
/* -------------------------------------------------------------------- */

void display_apparition(int array_size, double *array) {
    for (int i = 0; i < array_size; i++) {
        printf("Les individus de la classe %d ont une probabilité d'apparition de %2.5f\n", i + 1, array[i]); // Affichage des probabilités
    }
    printf("\n");
}

/* -------------------------------------------------------------------- */
/* generic_discrite_distrib                                             */
/*                                                                      */
/* Calcule les probabilités de classes et les probabilités cumulées à   */
/* partir d'un tableau de distribution.                                 */
/*                                                                      */
/* En entrée :                                                          */
/*   - int array_size : taille du tableau de distribution               */
/*   - int *array : tableau des occurrences des classes                 */
/*   - double *class_proba : tableau pour stocker les probabilités      */
/*   - double *cumulativ_proba : tableau pour stocker les cumuls        */
/*                                                                      */
/* En sortie :                                                          */
/*   - Remplit les tableaux class_proba et cumulativ_proba.             */
/* -------------------------------------------------------------------- */

void generic_discrite_distrib(int array_size, int *array, double *class_proba, double *cumulativ_proba) {
    int tot_individual = 0; 
 
    //Calcul du nombre total d'individus préents dans les classes
    for (int i = 0; i < array_size; i++) {
        tot_individual += array[i]; 
    }

    for (int i = 0; i < array_size; i++) {
        class_proba[i] = ((double)array[i] / (double)tot_individual); // Calcul de la probabilité de chaque classe
        cumulativ_proba[i] = (i > 0 ? cumulativ_proba[i - 1] : 0) + class_proba[i]; // Calcul des probabilités cumulées
    }
}

/* -------------------------------------------------------------------- */
/* class_simulation                                                     */
/*                                                                      */
/* Simule un nombre donné d'événements pour une classe donnée.          */
/*                                                                      */
/* En entrée :                                                          */
/*   - int array_size : taille du tableau de distribution               */
/*   - int nb_drawings : nombre de tirages à effectuer                  */
/*   - int *distribution : tableau pour stocker la distribution         */
/*                                                                      */
/* En sortie :                                                          */
/*   - Remplit le tableau distribution.                                 */
/* -------------------------------------------------------------------- */

void class_simulation(int array_size, int nb_drawings, int *distribution) {
    double indiv = 0; // Variable pour stocker les valeurs générées
    double frequency = (1 / (double)array_size); // Fréquence pour chaque classe

    for (int i = 0; i < (nb_drawings / array_size); i++) {
        indiv = genrand64_real1(); // Génération d'un nombre aléatoire
        
        for (int j = 0; j < array_size; j++) {
            if (indiv >= j * frequency && indiv < (j + 1) * frequency) {
                distribution[j] += 1; // Incrémentation pour la classe correspondante
            }
        }
    }
}

/* -------------------------------------------------------------------- */
/* neg_exp                                                              */
/*                                                                      */
/* Génère un échantillon d'une loi exponentielle négative avec une      */
/* moyenne donnée.                                                      */
/*                                                                      */
/* En entrée :                                                          */
/*   - double mean : moyenne de la loi exponentielle                    */
/*                                                                      */
/* En sortie :                                                          */
/*   - Retourne une valeur générée suivant une loi exponentielle.       */
/* -------------------------------------------------------------------- */

double neg_exp(double mean) {
    return -mean * log(genrand64_real1()); // Génération d'une valeur exponentielle
}

/* -------------------------------------------------------------------- */
/* neg_exp_simu                                                         */
/*                                                                      */
/* Simule des événements d'une loi exponentielle négative.              */
/*                                                                      */
/* En entrée :                                                          */
/*   - int nb_drawings : nombre d'événements à simuler                  */
/*   - double mean : moyenne de la loi exponentielle                    */
/*                                                                      */
/* En sortie :                                                          */
/*   - Retourne la moyenne des valeurs générées.                        */
/* -------------------------------------------------------------------- */

double neg_exp_simu(int nb_drawings, double mean) {
    int sum = 0; // Somme des valeurs générées
    
    for (int i = 0; i < nb_drawings; i++) {
        sum += neg_exp(mean); // Ajout d'une nouvelle valeur
    }
    
    return ((double)sum / (double)nb_drawings); // Calcul de la moyenne
}

/* -------------------------------------------------------------------- */
/* neg_exp_distrib                                                      */
/*                                                                      */
/* Distribue des échantillons d'une loi exponentielle négative dans un  */
/* tableau de bins.                                                     */
/*                                                                      */
/* En entrée :                                                          */
/*   - int *Test22bins : tableau pour stocker les occurrences           */
/*   - int nb_drawings : nombre d'événements à simuler                  */
/*   - double mean : moyenne de la loi exponentielle                    */
/*                                                                      */
/* En sortie :                                                          */
/*   - Remplit le tableau Test22bins.                                   */
/* -------------------------------------------------------------------- */

void neg_exp_distrib(int *Test22bins, int nb_drawings, double mean) {
    double neg = 0; // Variable pour stocker la valeur générée
    
    for (int i = 0; i < nb_drawings; i++) {
        neg = neg_exp(mean); // Génération d'une valeur exponentielle
        
        (((int)neg > 20) ? Test22bins[20]++ : Test22bins[(int)neg]++); // Incrémentation pour les bins
    }
}

/* -------------------------------------------------------------------- */
/* display_neg_exp_distrib                                              */
/*                                                                      */
/* Affiche la distribution des valeurs d'une loi exponentielle négative.*/
/*                                                                      */
/* En entrée :                                                          */
/*   - int *Test22bins : tableau des occurrences                        */
/*                                                                      */
/* En sortie :                                                          */
/*   - Affiche la distribution des occurrences.                         */
/* -------------------------------------------------------------------- */

void display_neg_exp_distrib(int *Test22bins) {
    for (int i = 0; i < 20; i++) {
        printf("Son apparues %d occurrences du nombre %d\n", Test22bins[i], i); // Affichage des occurrences pour chaque bin
    }
    
    printf("Son apparues %d occurrences du nombre 20+\n\n", Test22bins[20]); // Affichage pour les valeurs supérieures à 20
}

/* -------------------------------------------------------------------- */
/* average_dice                                                         */
/*                                                                      */
/* Calcule la moyenne de lancers de dés sur un nombre donné de          */
/* tirages.                                                             */
/*                                                                      */
/* En entrée :                                                          */
/*   - int nb_drawing : nombre de tirages à effectuer                   */
/*                                                                      */
/* En sortie :                                                          */
/*   - double *mean : pointeur vers la moyenne des résultats            */
/* -------------------------------------------------------------------- */

void average_dice(int nb_drawing, double *mean) {
    int sum = 0; // Somme des lancers de dés
    
    for (int i = 0; i < nb_drawing; i++) {
        sum += ((int)(genrand64_int64()>>32)) % 6 + 1; // Lancé de dé entre 1 et 6
    }
    
    *mean = (double)sum / (double)nb_drawing; // Calcul de la moyenne
    *mean = round(100. * (*mean)) / 100.; // Arrondi à deux décimales
}

/* -------------------------------------------------------------------- */
/* array_average_dice                                                   */
/*                                                                      */
/* Calcule la moyenne des lancers de dés et stocke les résultats        */
/* dans un tableau.                                                     */
/*                                                                      */
/* En entrée :                                                          */
/*   - int nb_drawing : nombre de tirages à effectuer                   */
/*   - double *mean : pointeur vers la moyenne actuelle                 */
/*   - double *array : tableau pour stocker les moyennes                */
/*   - int array_size : taille du tableau                               */
/*   - double *array_mean : pointeur vers la moyenne du tableau         */
/*                                                                      */
/* En sortie :                                                          */
/*   - Remplit le tableau avec les moyennes calculées.                  */
/* -------------------------------------------------------------------- */

void array_average_dice(int nb_drawing, double *mean, double *array, int array_size, double *array_mean) {
    for (int i = 0; i < array_size; i++) {
        average_dice(nb_drawing, mean); // Appel à la fonction pour calculer la moyenne
        array[i] = *mean; // Stockage de la moyenne
        *array_mean += *mean; // Somme des moyennes
    }
    
    *array_mean = *array_mean / ((double)array_size); // Calcul de la moyenne globale
}

/* -------------------------------------------------------------------- */
/* array_to_excel                                                       */
/*                                                                      */
/* Écrit les données d'un tableau dans un fichier Excel.                */
/*                                                                      */
/* En entrée :                                                          */
/*   - int *freq_array : tableau des fréquences                         */
/*   - int array_size : taille du tableau                               */
/*   - char *file_name : nom du fichier ou écrire                       */
/*   - double array_mean : moyenne des résultats                        */
/*   - double *dice_array : tableau des résultats de dés                */
/*                                                                      */
/* En sortie :                                                          */
/*   - Écrit les données dans un fichier Excel.                         */
/* -------------------------------------------------------------------- */

void array_to_excel(int *freq_array, int array_size, char *file_name, double array_mean, double *dice_array) {
    FILE *file = fopen(file_name, "w"); // Ouverture du fichier

    fprintf(file, "%d,%f,%f\n", freq_array[0], dice_array[0], array_mean); // Écriture de la première ligne

    for (int i = 1; i < array_size; i++) {
        fprintf(file, "%d,%f\n", freq_array[i], dice_array[i]); // Écriture des lignes suivantes
    }
    
    fclose(file); // Fermeture du fichier
}

/* -------------------------------------------------------------------- */
/* array_to_freq                                                        */
/*                                                                      */
/* Calcule la distribution des fréquences des valeurs d'un tableau.     */
/*                                                                      */
/* En entrée :                                                          */
/*   - double *array : tableau des valeurs à distribuer                 */
/*   - int array_size : taille du tableau                               */
/*   - int *freq_array : tableau pour stocker les fréquences            */
/*   - int freq_array_size : taille du tableau de fréquences            */
/*   - double min_val : valeur minimale pour la distribution            */
/*   - double max_val : valeur maximale pour la distribution            */
/*                                                                      */
/* En sortie :                                                          */
/*   - Remplit le tableau de fréquences avec les occurrences.           */
/* -------------------------------------------------------------------- */

void array_to_freq(double *array, int array_size, int *freq_array, int freq_array_size, double min_val, double max_val) {
    double freq = 0; // Valeur courante
    double freq_step = ((max_val - min_val) / (double)freq_array_size); // Calcul de l'intervalle

    for (int i = 0; i < array_size; i++) {
        freq = array[i]; // Récupération de la valeur courante

        for (int j = 0; j < freq_array_size; j++) {
            if (freq >= min_val + (((double)j) * freq_step) && freq < min_val + (((double)j + 1) * freq_step)) {
                freq_array[j]++; // Incrémentation de la fréquence pour la classe correspondante
            }
            array[j] = min_val + (((double)j) * freq_step); // Mise à jour du tableau
        }
    }
}

/* -------------------------------------------------------------------- */
/* box_and_muller                                                        */
/*                                                                      */
/* Génère deux nombres aléatoires suivant une distribution normale.     */
/*                                                                      */
/* En entrée :                                                          */
/*   - double *Rn1 : pointeur pour le premier nombre                    */
/*   - double *Rn2 : pointeur pour le deuxième nombre                   */
/*                                                                      */
/* En sortie :                                                          */
/*   - Remplit Rn1 et Rn2 avec les valeurs générées.                    */
/* -------------------------------------------------------------------- */

void box_and_muller(double *Rn1, double *Rn2) {
    *Rn1 = (genrand64_real2() - 1) * (-1); // Assure que Rn1 est dans (0, 1]
    *Rn2 = (genrand64_real2() - 1) * (-1); // Assure que Rn2 est dans (0, 1]

    *Rn1 = cos(2 * PI * (*Rn2)) * pow( (((double)-2 )* log(*Rn1)), (double)(1 / 2)); // Transformation de Box-Muller
    *Rn2 = sin(2 * PI * (*Rn2)) * pow( (((double)-2 ) * log(*Rn1)), (double)(1 / 2)); // Transformation de Box-Muller

    *Rn1 *= 5; // Mise à l'échelle
    *Rn2 *= 5; // Mise à l'échelle
}

/* -------------------------------------------------------------------- */
/* box_muller_array                                                      */
/*                                                                      */
/* Génère un tableau de nombres aléatoires suivant une distribution     */
/* normale à partir de la méthode de Box-Muller.                        */
/*                                                                      */
/* En entrée :                                                          */
/*   - double *array : tableau pour stocker les valeurs                 */
/*   - int nb_iter : nombre d'itérations à effectuer                    */
/*   - double *Rn1 : pointeur pour le premier nombre                    */
/*   - double *Rn2 : pointeur pour le deuxième nombre                   */
/*                                                                      */
/* En sortie :                                                          */
/*   - Remplit le tableau avec les valeurs générées.                    */
/* -------------------------------------------------------------------- */

void box_muller_array(double *array, int nb_iter, double *Rn1, double *Rn2) {
    for (int i = 0; i < (nb_iter / 2); i++) {
        box_and_muller(Rn1, Rn2); // Appel à la fonction de Box-Muller
        array[2 * i] = *Rn1; // Stockage du premier nombre
        array[(2 * i) + 1] = *Rn2; // Stockage du deuxième nombre
    }
}


int main() {

    //Question 2)
    //printf("Question 2)\n");

    //printf("100 generations ave ensemble de temperatures comprises entre -98 et 57.5");
    uniform(-98,57.7);

    
    //Question 3.1) 
    printf("Question 3.1)\n");

    int nb_drawings = 1000;
    discrete_distribution(nb_drawings);

    nb_drawings = 10000;
    discrete_distribution(nb_drawings);

    nb_drawings = 100000;
    discrete_distribution(nb_drawings);

    nb_drawings = 1000000;
    discrete_distribution(nb_drawings);

    //Question 3.2)
    printf("Question 3.2)\n");


    int array_size = 5;
    int * distribution;
    double * class_proba;
    double * cumulativ_proba;
    distribution = calloc(array_size, sizeof(int));
    class_proba = calloc(array_size, sizeof(double));
    cumulativ_proba = calloc(array_size, sizeof(double));
    int nb_drawing = 1000;


    class_simulation(array_size, nb_drawing ,distribution);
    generic_discrite_distrib( array_size, distribution, class_proba, cumulativ_proba);

    //Question 3.2.a)
    printf("Question 3.2.a)\n");

    printf("Probabilités de classes pour %d classes  et %d drawings:\n", array_size, nb_drawing);
    display_apparition(array_size,class_proba );
    

    //Question 3.2.b)
    printf("Question 3.2.b)\n");

    nb_drawing = 1000;
    class_simulation(array_size, nb_drawing ,distribution);
    generic_discrite_distrib( array_size, distribution, class_proba, cumulativ_proba);

    printf("Probabilités cumulées de classes pour %d classes et %d drawings:\n", array_size, nb_drawing);
    display_apparition(array_size, cumulativ_proba );

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    nb_drawing = 10000;
    class_simulation(array_size, nb_drawing ,distribution);
    generic_discrite_distrib( array_size, distribution, class_proba, cumulativ_proba);

    printf("Probabilités cumulées de classes pour %d classes et %d drawings:\n", array_size, nb_drawing);
    display_apparition(array_size, cumulativ_proba );

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    nb_drawing = 100000;
    class_simulation(array_size, nb_drawing ,distribution);
    generic_discrite_distrib( array_size, distribution, class_proba, cumulativ_proba);

    printf("Probabilités cumulées de classes pour %d classes et %d drawings:\n", array_size, nb_drawing);
    display_apparition(array_size, cumulativ_proba );


    

    //Question 3.2.c)
    printf("Question 3.2.c)\n");

    int distribution_slides[6] = {1,4,6,4,1,2};
    array_size = 6;
    nb_drawing = 1000;
    class_proba = calloc(array_size, sizeof(double));
    cumulativ_proba = calloc(array_size, sizeof(double));

    generic_discrite_distrib( array_size, distribution_slides, class_proba, cumulativ_proba);
    printf("Probabilités cumulées de classes pour %d classes\n", array_size);
    display_apparition(array_size, cumulativ_proba );









    //Question 4.b)

    printf("Question 4.b)\n");


    nb_drawings = 1000;
    int mean = 10;
    printf("Simulation de %d évènements de loi exponentielle de moyenne %d\n", nb_drawings, mean);
    printf("La moyenne des évènements est de %f\n", neg_exp_simu(nb_drawings, mean));
    
    nb_drawings = 1000000;
    printf("Simulation de %d évènements de loi exponentielle de moyenne %d\n", nb_drawings, mean);
    printf("La moyenne des évènements est de %f\n", neg_exp_simu(nb_drawings, mean));




    //Question 4.c)
    printf("Question 4.c)\n");

    
    int * Test22bins = calloc(21, sizeof(int));


    nb_drawings = 1000;
    mean = 10;
    neg_exp_distrib(Test22bins, nb_drawings, mean);
    display_neg_exp_distrib(Test22bins);


    for (int i = 0; i < 21; i++){
        Test22bins[i] = 0;
    }

    nb_drawings = 1000000;
    mean = 10;
    neg_exp_distrib(Test22bins, nb_drawings, mean);
    display_neg_exp_distrib(Test22bins);

    char * file_name ="../.txt/neg_exp_distrib.txt";

    FILE *file = fopen(file_name, "w"); // Ouverture du fichier

    for (int i = 0; i<19; i++){ 
    fprintf(file, "%d\n",Test22bins[i]);
    }
    fprintf(file, "%d",Test22bins[19]);


    //Question 5.1)
    printf("Question 5.1)\n");

    // approximation de la moyenne du lancé nb_drawing fois 
    double avg = 0;
    nb_drawings = 8;
    average_dice(nb_drawings, &avg);
    printf("La moyenne de la somme des dés est de %f pour %d drawing\n", avg, nb_drawing);



    //création d'une liste de taille 1000 de moyennes sur nb_lancés

    avg = 0;
    double array_mean = 0;
    double * dice_array ;
    int * freq_array ;


    dice_array = calloc(1000, sizeof(double));
    freq_array = calloc(241, sizeof(int));



    array_average_dice(1000, &avg,dice_array, 1000, &array_mean);

    // Transformation de l'array en array des fréquences d'appartition sur 241 valeurs

    array_to_freq(dice_array, 1000, freq_array, 241, 0,42);

    // Ecriture dans un fichier .xlsx des données calculées pour plot

    array_to_excel(freq_array, 241, "../.xlsx/Dice_Sim.xlsx", array_mean, dice_array);


    //Question 5.2)
    //printf("Question 5.2)\n");

    //En suivant le même procédé que pour la génération et le plot de données des dés on crée ne suites de valeur suivant la loi Box an Muller 

    double Rn1 = 0;
    double Rn2 = 0;

    int nb_iter = 1000000;
    int bins = 20;

    double * array_BAM;


    freq_array = calloc(20, sizeof(int));



    array_BAM = calloc(nb_iter, sizeof(double));

    box_muller_array(array_BAM, nb_iter, &Rn1, &Rn2);

    // On transforme l'array des valeurs en array des fréquences d'apprition sur bins valeurs

    array_to_freq(array_BAM, nb_iter, freq_array, bins, -5, 5);

    array_to_excel(freq_array,bins,"../.xlsx/BAM_Simu.xlsx",0,array_BAM);



    
    


    return 0;
}